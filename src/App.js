import React from 'react';
import logo from './logo.svg';
import './App.css';


const PlayerCard =(props)=> {
  return(
    <div className = "player-card">
      {props.symbol}
    </div>
  )
}

class App extends React.Component {
 render(){
   return(
    <div className= "App">
      <PlayerCard 
        color = "red"
        symbol= "paper" />
      <PlayerCard 
          color = "blue"
          symbol="rock" />

    </div>
     
  );
}
}

export default App;
